###############################################################################
# Find FLANN
#
# This sets the following variables:
# FLANN_FOUND - True if FLANN was found.
# FLANN_INCLUDE_DIRS - Directories containing the FLANN include files.
# FLANN_LIBRARIES - Libraries needed to use FLANN.
# FLANN_DEFINITIONS - Compiler flags for FLANN.
# If FLANN_USE_STATIC is specified and then look for static libraries ONLY else
# look for shared ones

set(FLANN_USE_STATIC ON)
# if(FLANN_USE_STATIC)
#   set(FLANN_RELEASE_NAME flann_cpp_s)
#   set(FLANN_DEBUG_NAME flann_cpp_s-gd)
# else(FLANN_USE_STATIC)
#   set(FLANN_RELEASE_NAME flann_cpp)
#   set(FLANN_DEBUG_NAME flann_cpp-gd)
# endif(FLANN_USE_STATIC)

# find_package(PkgConfig QUIET)
# if (FLANN_FIND_VERSION)
#     pkg_check_modules(PC_FLANN flann>=${FLANN_FIND_VERSION})
# else(FLANN_FIND_VERSION)
#     pkg_check_modules(PC_FLANN flann)
# endif(FLANN_FIND_VERSION)

# set(FLANN_DEFINITIONS ${PC_FLANN_CFLAGS_OTHER})

# find_path(FLANN_INCLUDE_DIR flann/flann.hpp
#           HINTS ${PC_FLANN_INCLUDEDIR} ${PC_FLANN_INCLUDE_DIRS} "${FLANN_ROOT}" "$ENV{FLANN_ROOT}"
#           PATHS "$ENV{PROGRAMFILES}/Flann" "$ENV{PROGRAMW6432}/Flann" 
#           PATH_SUFFIXES include)

# find_library(FLANN_LIBRARY
#              NAMES ${FLANN_RELEASE_NAME}
#              HINTS ${PC_FLANN_LIBDIR} ${PC_FLANN_LIBRARY_DIRS} "${FLANN_ROOT}" "$ENV{FLANN_ROOT}"
#              PATHS "$ENV{PROGRAMFILES}/Flann" "$ENV{PROGRAMW6432}/Flann" 
# 	     PATH_SUFFIXES lib)

# find_library(FLANN_LIBRARY_DEBUG 
#              NAMES ${FLANN_DEBUG_NAME} ${FLANN_RELEASE_NAME}
# 	     HINTS ${PC_FLANN_LIBDIR} ${PC_FLANN_LIBRARY_DIRS} "${FLANN_ROOT}" "$ENV{FLANN_ROOT}"
# 	     PATHS "$ENV{PROGRAMFILES}/Flann" "$ENV{PROGRAMW6432}/Flann" 
# 	     PATH_SUFFIXES lib)

# if(NOT FLANN_LIBRARY_DEBUG)
#   set(FLANN_LIBRARY_DEBUG ${FLANN_LIBRARY})
# endif(NOT FLANN_LIBRARY_DEBUG)

# set(FLANN_INCLUDE_DIRS ${FLANN_INCLUDE_DIR})
# set(FLANN_LIBRARIES optimized ${FLANN_LIBRARY} debug ${FLANN_LIBRARY_DEBUG})
set(FLANN_INCLUDE_DIR ${CMAKE_SOURCE_DIR}/3rdparty/flann/src/cpp)
set(FLANN_LIBRARY flann_cpp_s)

# include(FindPackageHandleStandardArgs)
# find_package_handle_standard_args(FLANN DEFAULT_MSG FLANN_LIBRARY FLANN_INCLUDE_DIR)

# find_library(FLANN_LIBRARY_SHARED
#   NAMES
#     flann_cpp
#   HINTS
#     ${PC_FLANN_LIBRARY_DIRS}
#     ${FLANN_ROOT}
#     $ENV{FLANN_ROOT}
#   PATHS
#     $ENV{PROGRAMFILES}/Flann
#     $ENV{PROGRAMW6432}/Flann
#   PATH_SUFFIXES
#     lib
# )

# find_library(FLANN_LIBRARY_DEBUG_SHARED
#   NAMES
#     flann_cpp-gd flann_cppd
#   HINTS
#     ${PC_FLANN_LIBRARY_DIRS}
#     ${FLANN_ROOT}
#     $ENV{FLANN_ROOT}
#   PATHS
#     $ENV{PROGRAMFILES}/Flann
#     $ENV{PROGRAMW6432}/Flann
#   PATH_SUFFIXES
#     lib
# )

# find_library(FLANN_LIBRARY_STATIC
#   NAMES
#     flann_cpp_s
#   HINTS
#     ${PC_FLANN_LIBRARY_DIRS}
#     ${FLANN_ROOT}
#     $ENV{FLANN_ROOT}
#   PATHS
#     $ENV{PROGRAMFILES}/Flann
#     $ENV{PROGRAMW6432}/Flann
#   PATH_SUFFIXES
#     lib
# )

# find_library(FLANN_LIBRARY_DEBUG_STATIC
#   NAMES
#     flann_cpp_s-gd flann_cpp_sd
#   HINTS
#     ${PC_FLANN_LIBRARY_DIRS}
#     ${FLANN_ROOT}
#     $ENV{FLANN_ROOT}
#   PATHS
#     $ENV{PROGRAMFILES}/Flann
#     $ENV{PROGRAMW6432}/Flann
#   PATH_SUFFIXES
#     lib
# )

# if(FLANN_LIBRARY_SHARED AND FLANN_LIBRARY_STATIC)
#   if(PCL_FLANN_REQUIRED_TYPE MATCHES "DONTCARE")
#     if(PCL_SHARED_LIBS)
#       set(FLANN_LIBRARY_TYPE SHARED)
#       set(FLANN_LIBRARY ${FLANN_LIBRARY_SHARED})
#     else()
#       set(FLANN_LIBRARY_TYPE STATIC)
#       set(FLANN_LIBRARY ${FLANN_LIBRARY_STATIC})
#     endif()
#   elseif(PCL_FLANN_REQUIRED_TYPE MATCHES "SHARED")
#     set(FLANN_LIBRARY_TYPE SHARED)
#     set(FLANN_LIBRARY ${FLANN_LIBRARY_SHARED})
#   else()
#     set(FLANN_LIBRARY_TYPE STATIC)
#     set(FLANN_LIBRARY ${FLANN_LIBRARY_STATIC})
#   endif()
# elseif(FLANN_LIBRARY_STATIC)
#   set(FLANN_LIBRARY_TYPE STATIC)
#   set(FLANN_LIBRARY ${FLANN_LIBRARY_STATIC})
# elseif(FLANN_LIBRARY_SHARED)
#   set(FLANN_LIBRARY_TYPE SHARED)
#   set(FLANN_LIBRARY ${FLANN_LIBRARY_SHARED})
# endif()

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(
  FLANN DEFAULT_MSG
  FLANN_LIBRARY FLANN_INCLUDE_DIR
)

set(FLANN_LIBRARY_TYPE STATIC)
set(FLANN_FOUND TRUE)
if(FLANN_FOUND)
  add_library(FLANN::FLANN ALIAS flann_cpp_s)
  include_directories(${FLANN_INCLUDE_DIR})
  # add_library(FLANN::FLANN ${FLANN_LIBRARY_TYPE} IMPORTED)
  # set_target_properties(FLANN::FLANN PROPERTIES INTERFACE_INCLUDE_DIRECTORIES "${FLANN_INCLUDE_DIR}")
  # set_target_properties(FLANN::FLANN PROPERTIES INTERFACE_COMPILE_DEFINITIONS "${PC_FLANN_CFLAGS_OTHER}")
  # set_property(TARGET FLANN::FLANN APPEND PROPERTY IMPORTED_CONFIGURATIONS "RELEASE")
  # set_target_properties(FLANN::FLANN PROPERTIES IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX")
  # if(WIN32 AND (NOT FLANN_LIBRARY_TYPE MATCHES "STATIC"))
  #   set_target_properties(FLANN::FLANN PROPERTIES IMPORTED_IMPLIB_RELEASE "${FLANN_LIBRARY}")
  # else()
  #   set_target_properties(FLANN::FLANN PROPERTIES IMPORTED_LOCATION_RELEASE "${FLANN_LIBRARY}")
  # endif()
  # if(FLANN_LIBRARY_DEBUG)
  #   set_property(TARGET FLANN::FLANN APPEND PROPERTY IMPORTED_CONFIGURATIONS "DEBUG")
  #   set_target_properties(FLANN::FLANN PROPERTIES IMPORTED_LINK_INTERFACE_LANGUAGES_DEBUG "CXX")
  #   if(WIN32 AND (NOT FLANN_LIBRARY_TYPE MATCHES "STATIC"))
  #     set_target_properties(FLANN::FLANN PROPERTIES IMPORTED_IMPLIB_DEBUG "${FLANN_LIBRARY_DEBUG}")
  #   else()
  #     set_target_properties(FLANN::FLANN PROPERTIES IMPORTED_LOCATION_DEBUG "${FLANN_LIBRARY_DEBUG}")
  #   endif()
  # endif()
  # # Pkgconfig may specify additional link libraries besides from FLANN itself
  # # in PC_FLANN_LIBRARIES, add them to the target link interface.
  # foreach(_library ${PC_FLANN_LIBRARIES})
  #   if(NOT _library MATCHES "flann")
  #     set_property(TARGET FLANN::FLANN APPEND PROPERTY INTERFACE_LINK_LIBRARIES "${_library}")
  #   endif()
  # endforeach()
  # get_filename_component(FLANN_ROOT "${FLANN_INCLUDE_DIR}" PATH)
endif()
